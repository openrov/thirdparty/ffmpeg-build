from cpt.packager import ConanMultiPackager

builder = ConanMultiPackager()
builder.add(settings={"build_type": "Debug"}, options={}, env_vars={}, build_requires={})
builder.add(settings={"build_type": "Release"}, options={}, env_vars={}, build_requires={})
builder.run()